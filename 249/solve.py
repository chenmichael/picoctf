#!/usr/bin/env python3
from level5 import *

with open('dictionary.txt', 'r') as f:
	pos_pw_list = f.read().split('\n')
correct_pw = next(pw for pw in pos_pw_list if hash_pw(pw) == correct_pw_hash)
print(f'Correct password is: {correct_pw}')
print(str_xor(flag_enc.decode(), correct_pw))